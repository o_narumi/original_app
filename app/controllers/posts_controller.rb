class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "投稿が完了しました"
      redirect_to current_user
    else
      flash[:danger] = "投稿内容に不足があります"
      redirect_to current_user
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "投稿が削除されました"
    redirect_to request.referrer || root_url
  end
  
  def index
    @post = Post.all
    @posts = Post.page(params[:page]).per(10).search(params[:search])
  end
  
  def liked_index
    @user = current_user
    @likes = Like.where(user_id: @user.id)
  end

  private

    def post_params
      params.require(:post).permit(:content, :title, :picture, :rate, :tag_list, :date, :weather)
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end